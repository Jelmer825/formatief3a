import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Random;

public class Reader {

    private final ArrayList<String> stringList = new ArrayList<String>();

    public Reader(int length){
       this.generateRandomStrings(length);
       this.sortStrings();
    }

    public void generateRandomStrings(int length){
        String CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        for (int j = 0; j < length - 1; j++){
            Random rnd = new Random();
            Character rchar = CHARS.charAt(rnd.nextInt(CHARS.length()));
            String charString = String.valueOf(rchar);
            this.stringList.add(charString);
        }
    }

    public void sortStrings()
    {
        String temp;
        int n = this.stringList.size();

        // Sorting strings using bubble sort
        for (int j = 0; j < n - 1; j++)
        {
            for (int i = j + 1; i < n; i++)
            {
                if (this.stringList.get(j).compareTo(this.stringList.get(i)) > 0)
                {
                    temp = this.stringList.get(j);
                    this.stringList.set(j, this.stringList.get(i));
                    this.stringList.set(i, temp);
                }
            }
        }

        System.out.println(this.stringList);
    }
}
